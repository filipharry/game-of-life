﻿namespace GameOfLife
{
    using System;
    using System.Threading;

    public class GameOfLife
    {
        private int Rows;
        private int Columns;
        private int Generation;
        private bool[,] Grid;

        const bool ALIVE = true;
        const bool DEAD = false;

        public GameOfLife(int rows, int columns)
        {
            this.Rows = rows;
            this.Columns = columns;
            Grid = new bool[rows, columns];
            SeedLife();
        }

        public void Run()
        {
            Generation++;

            Console.WriteLine("Generation {0}:\n4,8\n", Generation);

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    Console.Write(Grid[i, j] ? "*" : ".");
                }
                Console.WriteLine();
            }
            CalculateNextGeneration();
        }

        private void CalculateNextGeneration()
        {
            var nextGenGrid = Grid.Clone() as bool[,];

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    int aliveNeighbours = GetAliveNeighbours(i, j);
                    bool isCellAlive = Grid[i, j];
                    nextGenGrid[i, j] = Policy(isCellAlive, aliveNeighbours);
                }
            }
            Grid = nextGenGrid.Clone() as bool[,];
        }

        private int GetAliveNeighbours(int x, int y)
        {
            int aliveNeighbours = 0;

            for (int i = x - 1; i < x + 2; i++)
            {
                for (int j = y - 1; j < y + 2; j++)
                {
                    if (!((i < 0 || j < 0) || (i >= Rows || j >= Columns)))
                    {
                        if (i == x && j == y)
                            continue;

                        if (Grid[i, j])
                            aliveNeighbours++;
                    }
                }
            }
            return aliveNeighbours;
        }

        private bool Policy(bool isCellAlive, int aliveNeighbours)
        {
            if ((isCellAlive && aliveNeighbours >= 2 && aliveNeighbours < 4) || (!isCellAlive && aliveNeighbours == 3))
            {
                return ALIVE;
            }
            return DEAD;
        }

        private void SeedLife()
        {
            // random mönster - påhittat
            //Grid[1, 1] = true;
            //Grid[1, 2] = true;
            //Grid[2, 2] = true;
            //Grid[2, 3] = true;

            // Block
            //Grid[1, 4] = true;
            //Grid[2, 3] = true;
            //Grid[2, 4] = true;

            // Blinker 
            Grid[1, 2] = true;
            Grid[1, 3] = true;
            Grid[1, 4] = true;

            // Toad
            //Grid[2, 3] = true;
            //Grid[2, 4] = true;
            //Grid[2, 5] = true;
            //Grid[1, 2] = true;
            //Grid[1, 3] = true;
            //Grid[1, 4] = true;
        }
    }

    internal class Program
    {
        static void Main()
        {
            int runs = 0;
            int maxRuns = 5;

            var gameOfLife = new GameOfLife(4, 8);

            while (runs++ < maxRuns)
            {
                gameOfLife.Run();
                Thread.Sleep(2000);
            }
        }
    }
}