﻿namespace GameOfLife.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class PolicyTests
    {
        /* 
        GAME RULES
        1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
        2. Any live cell with more than three live neighbours dies, as if by overcrowding.
        3. Any live cell with two or three live neighbours lives on to the next generation.
        4. Any dead cell with exactly three live neighbours becomes a live cell.
        */

        const bool ALIVE = true;
        const bool DEAD = false;

        [TestMethod]
        public void LiveCell_LessThanTwoAliveNeighbours_Dies()
        {
            var cellState = ALIVE;
            int liveNeighbours = 0;

            var result = Rules.Policy(cellState, liveNeighbours);

            Assert.AreEqual(DEAD, result);
        }

        [TestMethod]
        public void LiveCell_WithMoreThenThreeAliveNeighbours_Dies()
        {
            var cellState = ALIVE;
            int liveNeighbours = 4;

            var result = Rules.Policy(cellState, liveNeighbours);

            Assert.AreEqual(DEAD, result);
        }

        [TestMethod]
        public void LiveCell_WithTwoAliveNeighbours_Lives()
        {
            var cellState = ALIVE;
            int liveNeighbours = 2;

            var result = Rules.Policy(cellState, liveNeighbours);

            Assert.AreEqual(ALIVE, result);
        }

        [TestMethod]
        public void LiveCell_WithThreeAliveNeighbours_Lives()
        {
            var cellState = ALIVE;
            int liveNeighbours = 3;

            var result = Rules.Policy(cellState, liveNeighbours);

            Assert.AreEqual(ALIVE, result);
        }

        [TestMethod]
        public void DeadCell_WithExactlyThreeAliveNeighbours_Lives()
        {
            var cellstate = DEAD;
            int liveNeighbours = 3;

            var result = Rules.Policy(cellstate, liveNeighbours);

            Assert.AreEqual(ALIVE, result);
        }
    }
}
