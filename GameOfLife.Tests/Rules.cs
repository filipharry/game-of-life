﻿namespace GameOfLife.Tests
{
    public static class Rules
    {
        public static bool Policy(bool isCellAlive, int aliveNeighbours)
        {
            if ((isCellAlive && aliveNeighbours >= 2 && aliveNeighbours < 4) || (!isCellAlive && aliveNeighbours == 3))
            {
                return true; // alive
            }
            return false; // dead
        }
    }
}
